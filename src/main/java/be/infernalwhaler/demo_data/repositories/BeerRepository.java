package be.infernalwhaler.demo_data.repositories;

import be.infernalwhaler.demo_data.model.Beer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository /* Equals value of a DAO class */
public interface BeerRepository extends JpaRepository<Beer, Integer> {

    List<Beer> getBeerByAlcohol(float percentage);

    List<Beer> findBeerByAlcoholOrName(float percentage, String name);

    // NamingConvention page 140
}

/*
* extends JpaRepository<Beer, Integer>
*     via This definition Spring creates behind the scenes and BeerRepositoryImpl behind the scenes
*     As @Repository is seen as a type DAO, this impl behind the scenes can be done!!
*     (This is not same for service class as we have to impl code ourselves for logic)
* */
package be.infernalwhaler.demo_data.repositories;

import be.infernalwhaler.demo_data.model.Brewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrewerRepository extends JpaRepository<Brewer, Integer> {

    /* This exists cause << NamingConventions >> && findBy || other convention names ...*/
    Brewer findByName(String name);


}

package be.infernalwhaler.demo_data.model;

import javax.persistence.*;

@Entity
@Table(name = "BeerOrderItem")
public class BeerOrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "Number")
    private int number;
    @ManyToOne
    @JoinColumn(name = "BeerId")
    private Beer beer;

    public int getId() {
        return id;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

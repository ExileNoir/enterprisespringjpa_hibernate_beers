package be.infernalwhaler.demo_data;

import be.infernalwhaler.demo_data.repositories.BeerRepository;
import be.infernalwhaler.demo_data.repositories.BrewerRepository;
import be.infernalwhaler.demo_data.services.BrewerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoDataApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DemoDataApplication.class, args);
        BeerRepository repoBeer = context.getBean(BeerRepository.class);
        BrewerRepository repoBrewer = context.getBean(BrewerRepository.class);
        System.out.println("SEARCHING FOR ID "+repoBeer.findById(4));

        BrewerService brewerService = context.getBean("brewerService",BrewerService.class);

        System.out.println("SEARCH BY NAME "+repoBrewer.findByName("Cnudde"));
        System.out.println("GET ALL BREWERS "+repoBrewer.findAll());
        System.out.println("SERVICE GET ALL "+brewerService.getAllBrewers());

        System.out.println("GET BEER BY ALCOHOL "+repoBeer.getBeerByAlcohol(7));

        System.out.println("QUERY OR PERCENTAGE OR NAME "+repoBeer.findBeerByAlcoholOrName(15,"Amelie"));

    }

}

/*
* Even though you get same Result if you query directly via << Repo >> interfaces as via << Service >>
*     this is seen as not clean coding practice (same as DAO vs Service)
* */
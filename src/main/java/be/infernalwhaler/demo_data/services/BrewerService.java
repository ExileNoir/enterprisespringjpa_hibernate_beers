package be.infernalwhaler.demo_data.services;

import be.infernalwhaler.demo_data.model.Brewer;
import be.infernalwhaler.demo_data.repositories.BrewerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrewerService {

    @Autowired
    private BrewerRepository brewerRepository;

    public Brewer getBrewerById(int id) {
        Optional<Brewer> optionalBrewer = brewerRepository.findById(id);
        if (optionalBrewer.isPresent()) {
            return optionalBrewer.get();
        } else {
            return null;
        }
    }

    public List<Brewer> getAllBrewers(){
        return brewerRepository.findAll();
    }

}

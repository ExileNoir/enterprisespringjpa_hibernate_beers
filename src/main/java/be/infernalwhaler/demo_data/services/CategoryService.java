package be.infernalwhaler.demo_data.services;

import be.infernalwhaler.demo_data.model.Category;
import be.infernalwhaler.demo_data.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category getCategoryById(int id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()) {
            return optionalCategory.get();
        } else {
            return null;
        }
    }

    public List<Category> getAllCategories(){
        return categoryRepository.findAll();
    }
}

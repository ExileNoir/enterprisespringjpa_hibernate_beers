package be.infernalwhaler.demo_data.services;

import be.infernalwhaler.demo_data.model.Beer;
import be.infernalwhaler.demo_data.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BeerService {

    @Autowired
    private BeerRepository beerRepository;

    public Beer getBeerById(int id) {
        Optional<Beer> optionalBeer = beerRepository.findById(id);
        if (optionalBeer.isPresent()) {
            return optionalBeer.get();
        } else {
            return null;
        }
    }
}

/*
 * OPTIONAL OBJECT
 * returned beerObject is being wrapped into an OptionalObject.
 * wrapper object that can verify is that wrapped object is null || not
 * */